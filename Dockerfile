FROM nginx:latest

COPY dist/game.js /usr/share/nginx/html
COPY dist/index.html /usr/share/nginx/html
COPY dist/assets /usr/share/nginx/html/assets

EXPOSE 8042

