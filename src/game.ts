import 'phaser';
import { GameObjects } from 'phaser';
import { Board } from './Board';
import { CardProvider } from './CardProvider';
import { Vector } from './Vector';

export default class Monarchy extends Phaser.Scene {
    board: Board;
    globalCardProvider: CardProvider;
    
    constructor() {
        super('Monarchy');
    }

    preload() {
        this.load.image('butterfly', 'assets/butterfly.png');
        this.load.image('butterfly1', 'assets/butterfly_blue.png');
        this.load.image('butterfly2', 'assets/butterfly_green.png');
        this.load.image('butterfly3', 'assets/butterfly_pink.png');
        this.load.image('reveal', 'assets/honey.png');
        this.load.image('bee', 'assets/bee.png');
        this.load.image('worm', 'assets/worm.png');
        this.load.image('bomb', 'assets/bomb.png');
        this.load.image('clock', 'assets/clock.png');
    }

    create() {
        // const butterfly = this.add.image(400, 300, 'butterfly');
        // this.tweens.add({
        //     targets: butterfly,
        //     y: 350,
        //     duration: 1500,
        //     ease: 'Sine.inOut',
        //     yoyo: true,
        //     repeat: -1
        // });

        this.globalCardProvider = new CardProvider();
        this.board = new Board(6, 4, this.globalCardProvider);
        console.log(this.board);

        const cellSize: Vector = this.GetMaxCellSize();
        const extraPadding: Vector = new Vector(
            config.width - (this.board.columns * cellSize.x),
            config.height - (this.board.rows * cellSize.y)
        );
        const beginx: number = (extraPadding.x / 2) + cellSize.x / 2;
        const beginy: number = (extraPadding.y / 2) + cellSize.y / 2;

        const butterfly = this.add.image(-100, -100, 'butterfly');

        const imageHeight = butterfly.height;
        const imageScale = Math.min(cellSize.x, cellSize.y) / imageHeight;

        for (let row = 0; row < this.board.squares.length; row++){
            for (let column = 0; column < this.board.squares[row].length; column++){
                let newImage = this.add.image(beginx + (row * cellSize.x), beginy + (column * cellSize.y), this.board.squares[row][column].card.suit);
                newImage.scaleY= imageScale;
                newImage.scaleX = imageScale;
            }
        }
    }

    // Returns a size that will ensure all cells are fully within the screen.
    GetMaxCellSize(): Vector {
        // let wide: boolean = this.board.columns > this.board.rows;
        // if (wide){
        //     let cellWidth: number = config.width / this.board.columns;
        //     let cellHeight: number = config.height / this.board.rows;
        //     while () {
        //         cellWidth--;
        //         cellHeight--;
        //     }
        //     let returnVec = new Vector();
        //     returnVec.x = cellWidth;
        //     returnVec.y = cellHeight;
        //     return returnVec; 
        // } else {
        //     let cellWidth: number = config.width / this.board.columns;
        //     let cellHeight: number = config.height / this.board.rows;
        //     while () {
        //     }
        //     let returnVec = new Vector();
        //     returnVec.x = cellWidth;
        //     returnVec.y = cellHeight;
        //     return returnVec;
        // }

        let cellWidth: number = config.width / this.board.columns;
        let cellHeight: number = config.height / this.board.rows;
        while (cellHeight * this.board.rows > config.height || cellWidth * this.board.columns > config.width) {
            cellWidth--;
            cellHeight--;
        }
        return new Vector(cellWidth, cellHeight);
    }

    update() {

    } 
}


const config = {
    type: Phaser.AUTO,
    backgroundColor: '#87ceeb',
    width: 1280,
    height: 720,
    scene: Monarchy,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {
                y: 300
            },
            debug: false,
        }
    }
};

const game = new Phaser.Game(config);