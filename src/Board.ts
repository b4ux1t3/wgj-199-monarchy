import { Card } from "./Card";
import { CardProvider } from "./CardProvider";

export class Board {
    squares: Square[][];
    cards: Card[];
    constructor(public columns: number, public rows: number, private cardProvider: CardProvider) {
        this.squares = []
        this.cards = this.cardProvider.getDeckOfCards( columns * rows);
        let index = 0;
        for (let i = 0; i < this.columns; i++){
            this.squares[i] = []
            for (let j = 0; j < this.rows; j++){
                this.squares[i][j] = new Square(this.cards[index++]);
            }
        }

    }
}

class Square {
    constructor(public card: Card){
        
    }
}