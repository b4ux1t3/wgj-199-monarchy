export class Asset {
    static kinds: string[] = ['IMAGE'];
    name: string;
    path: string;
    kind: string;
    constructor(name: string, path: string, kind: string) {
        if (!Asset.kinds.includes(kind)) throw new Error("Invalid asset kind!");
        this.name = name;
        this.path = path;
        this.kind = kind;
    }
}