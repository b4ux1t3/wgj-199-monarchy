export class Card {
    public static suits: string[] = [
        "butterfly1",   // Blue
        "butterfly2",   // Purple
        "butterfly3",   // Pink
        "bee",          // Bee. . .duh
        "bomb",         // Bug Bomb
        "worm",         // Earth Worm
        "clock",        // Clock
        "reveal"        // Honey
    ]
    suit: string;
    constructor(suit: string){
        if (!Card.suits.includes(suit)){
            console.log(`Invalid suit: ${suit}`);
            this.suit = null;
        } else {
            this.suit = suit;
        }

    }

}