import { Asset } from "./asset";
import { Configurator } from "./configurator";

export class AssetLoader {
    constructor(config : Configurator, scene: Phaser.Scene) {
        config.assets.forEach(asset => {
            if (asset.kind === "IMAGE"){
                scene.load.image(asset.name, asset.path);
            }
        });
    }
}