import { Card } from "./Card";

export class CardProvider {
    
    getDeckOfCards(size: number, butterflyPercent: number = 0.7, beePercent: number = 0.2, powerUpPercent: number = 0.1): Card[]{
        const returnArray: Card[] = [];
        // We need to have an even number of each of the types of butterflies, but we don't need to have an equal number of them!
        let butterflyColorCounts: number[] = [0, 0, 0]
        // We need to move on to the next butterfly color if we have hit two of one butterfly. We'll take butterflyCount % 3 to get which butterfly we should be on, and we'll take butterflyColorCount % 2 to figure out if we need to increment the count!
        let butterflyCount = 0;
        let butterflyColorCount = 0;

        let beeCount = 0;


        for (let i = 0; i < size; i++) {
            let diceRoll = Math.random();
            let newSuit: string;
            if (diceRoll < butterflyPercent){
                // Get the "color" of butter fly by appending the butterflyCount % 3 and adding 1 (giving us 1, 2 or 3)
                newSuit = "butterfly" + ((butterflyCount % 3) + 1);
                butterflyColorCounts[butterflyCount % 3]++;
                // Increment our colorCount. it it's odd, don't do anything. If it's even, increment butterflyCount. Gives us two of each color in a row.
                if (++butterflyColorCount % 2 === 0) butterflyCount++;
            } else if (diceRoll < butterflyPercent + beePercent) {
                newSuit = "bee";
                beeCount++;
            } else {
                const offset = Math.floor(Math.random() * 4); // 0-3
                newSuit = Card.suits[4 + offset]; // Should give us 4, 5, 6, or 7, 
            }

            const newCard = new Card(newSuit);

            returnArray.push(newCard);
        }
        // check our butterflyColorCounts, and if any of them are odd, we need to replace a bee with a butterfly of that color.
        for (let i = 0; i < butterflyColorCounts.length; i++) {
            if (butterflyColorCounts[i] % 2 !== 0) {
                const suitIsBee = (element: Card) => element.suit === "bee";
                const beeIndex = returnArray.findIndex(suitIsBee);
                returnArray[beeIndex] = new Card("butterfly" + (i + 1));
                butterflyColorCounts[i]++;
            }
        }
        // console.log(butterflyColorCounts);

        // We want to make sure there aren't too many bees!
        // It's okay if there are slightly too many powerups, 
        // but too many bees will make it very difficult!
        while (beeCount / returnArray.length > beePercent)
        {
            // console.log(`Bee count too high.\nTarget: ${beePercent * 100}%\nActual: ${(beeCount / returnArray.length * 100).toFixed(2)}%\nPruning a bee. . .`);
            const suitIsBee = (element: Card) => element.suit === "bee";
            const beeIndex = returnArray.findIndex(suitIsBee);
            const offset = Math.floor(Math.random() * 4); // 0-3
            const newSuit = Card.suits[4 + offset]; // Should give us 4, 5, 6, or 7,
            returnArray[beeIndex] = new Card(newSuit);
            beeCount--;
        }
        return returnArray;
    }

    constructor() {
        
    }
}