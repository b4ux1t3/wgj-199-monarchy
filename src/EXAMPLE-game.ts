import 'phaser';
import { GameObjects } from 'phaser';

export default class Demo extends Phaser.Scene {
    constructor() {
        super('demo');
    }

    platforms: Phaser.Physics.Arcade.StaticGroup;
    stars: Phaser.Physics.Arcade.Group;
    bombs: Phaser.Physics.Arcade.Group;
    player: Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
    cursors: Phaser.Types.Input.Keyboard.CursorKeys;
    score: number = 0;
    scoreText: GameObjects.Text;
    gameOver: boolean;

    /// PRELOAD METHODS ///
    loadSprites(): void {
        this.load.image('twinkly', 'assets/twinkly.png');
        this.load.image('butterfly', 'assets/butterfly.png');
        this.load.image('platform', 'assets/platform.png');
        this.load.image('ground', 'assets/ground.png');
        this.load.image('tutground', 'assets/tutorial assets/platform.png')
        this.load.image('sky', 'assets/tutorial assets/sky.png');
        this.load.image('star', 'assets/tutorial assets/star.png');
        this.load.image('bomb', 'assets/tutorial assets/bomb.png');
        this.load.spritesheet('dude',
            'assets/tutorial assets/dude.png', {
                frameWidth: 32,
                frameHeight: 48
            },
        );
    }

    preload() {
        this.loadSprites();
    }

    ///CREATE METHODS///
    generateWorld(): void {
        this.platforms = this.physics.add.staticGroup();
        this.platforms.create(400, 568, 'tutground').setScale(2).refreshBody();

        this.platforms.create(600, 400, 'tutground');
        this.platforms.create(50, 250, 'tutground');
        this.platforms.create(750, 220, 'tutground');
    }

    generatePlayer(): void {
        this.player = this.physics.add.sprite(100, 450, 'dude');
        this.player.setBounce(0.2);
        this.player.setCollideWorldBounds(true);
    }

    generateAnimations() {
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('dude', {
                start: 0,
                end: 3
            }),
            frameRate: 10,
            repeat: -1,
        });

        this.anims.create({
            key: 'turn',
            frames: [{
                key: 'dude',
                frame: 4
            }],
            frameRate: 20
        });

        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('dude', {
                start: 5,
                end: 8
            }),
            frameRate: 10,
            repeat: -1
        });
    }

    generatePhysics(): void {
        this.physics.add.collider(this.player, this.platforms);
        
        this.stars = this.physics.add.group({
            key: 'star',
            repeat: 11,
            setXY: { x: 12, y: 0, stepX: 70 },
            collideWorldBounds: true,
        });
        this.physics.add.collider(this.stars, this.platforms);
        
        this.stars.children.iterate((child) => {
            (child.body as Phaser.Physics.Arcade.Body).setBounce(1, 1);
        });

        this.physics.add.overlap(this.player, this.stars, (player, star) => {
            star.body.position.y = 1000000;
            star.setActive(false);
            this.score += 10; 
            this.scoreText.setText(`Score: ${this.score}`);

            if (this.stars.countActive(true) === 0) {
                console.log();
                this.stars.children.iterate((child) => {
                    console.log(child);
                    child.body.position.y = 0;
                    child.body.velocity.y = 0;
                    child.body.velocity.x = Phaser.Math.Between(-200, 200);
                    child.setActive(true);
                });

                this.spawnBomb();
            }
        }, null, this);

        this.bombs = this.physics.add.group();
        this.physics.add.collider(this.bombs, this.platforms);
        this.physics.add.collider(this.player, this.bombs, () => {
            this.physics.pause();
            this.player.setTint(0xff0000);
            this.player.anims.play('turn');
            this.gameOver = true;
        }, null, this);
    }

    spawnBomb() : void {
        let x = (this.player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

        let bomb = this.bombs.create(x, 16, 'bomb');
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
    }

    generateUI(): void {
        this.scoreText = this.add.text(16, 16, 'Score: 0', {fontSize: '32px', color: '#123'});
    }
    
    setupInputs(): void {
        this.cursors = this.input.keyboard.createCursorKeys();
    }

    initializeLevel() : void {
        this.generateWorld();
        this.generatePlayer();
        this.generateAnimations();
        this.generatePhysics();
        this.generateUI();
        this.setupInputs();
    }

    create() {
        this.initializeLevel();
    }

    /// UPDATE METHODS ///
    doInputs() {
        if (this.cursors.left.isDown){
            this.player.setVelocityX(-160);
            this.player.anims.play('left', true);
        } else if (this.cursors.right.isDown) {
            this.player.setVelocityX(160);
            this.player.anims.play('right', true);
        } else {
            this.player.setVelocityX(0);
            this.player.anims.play('turn', true);
        }

        if (this.cursors.up.isDown && this.player.body.touching.down){
            this.player.setVelocityY(-330);
        }
    }

    update() {
        this.doInputs();
    }

}

const config = {
    type: Phaser.AUTO,
    backgroundColor: '#87ceeb',
    width: 800,
    height: 600,
    scene: Demo,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {
                y: 300
            },
            debug: false,
        }
    }
};

const game = new Phaser.Game(config);